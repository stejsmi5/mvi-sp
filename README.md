# Klasifikace anomálií v lozích

Tato semsetrální práce se zabírá problematikou klasifikace logů. V první části bude probrána vhodná příprava vstupních dat, která data z jednotlivých záznamů logu vybrat, a která naopak nezpracovávat. Dále zde bude probráno jakým způsobem tato data zpracovat pro použití klasifikátoru v podobě neuronové sítě. 

Proces zpracování logů do trénovacího datasetu je popsán v notebooku prepare_data


**Předbežný předpoklad procesu učícího algoritmu:**  
Self-Organizing Map

**Dataset**  
<!--Plánuji použít tento [dataset](http://ita.ee.lbl.gov/html/contrib/NASA-HTTP.html), konkrétně data z července.-->